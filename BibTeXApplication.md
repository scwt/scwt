## Application Reference
 - JabRef - an open source bibliography reference manager - http://www.jabref.org/

## Formats Reference
 - BibTeX - http://www.bibtex.org/
 - IEEE Reference Guide - https://ieeeauthorcenter.ieee.org/wp-content/uploads/IEEE-Reference-Guide.pdf
 - IEEE Style - https://pitt.libguides.com/citationhelp/ieee
 
## Git Basics
 - Основи використання системи контролю версій Git - https://gitlab.com/itpm/itpmanagement/blob/master/Lab1.pdf