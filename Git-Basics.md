## Git Basics
 - Git SCM - https://git-scm.com/
 - Scott Chacon and Ben Straub, Pro Git - https://git-scm.com/book/en/v2
 - Основи використання системи контролю версій Git - https://gitlab.com/itpm/itpmanagement/blob/master/Lab1.pdf
 - git-submodule Documentation - https://git-scm.com/docs/git-submodule (
 [рос. версія статті](https://git-scm.com/book/ru/v2/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B-Git-%D0%9F%D0%BE%D0%B4%D0%BC%D0%BE%D0%B4%D1%83%D0%BB%D0%B8))
 - Организация работы с git submodules [Хабр] - https://habr.com/ru/post/60860/
 - Git subtree в деталях [Хабр] - https://habr.com/ru/post/429014/
 - Git subtree: the alternative to Git submodule - https://www.atlassian.com/git/tutorials/git-subtree