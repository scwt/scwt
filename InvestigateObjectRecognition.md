## Metrics
 - Intersection over Union (IoU) for object detection - https://www.pyimagesearch.com/2016/11/07/intersection-over-union-iou-for-object-detection/
 - mAP (mean Average Precision) for Object Detection - https://medium.com/@jonathan_hui/map-mean-average-precision-for-object-detection-45c121a31173

## Object Recognition
 - V. Parubochyi and R. Shuvar, "Comparison of Multi-Object Recognition Models" - http://elct.lnu.edu.ua/elit_conf/pdf/10/10_A28.pdf
 - Arthur Ouaknine, Review of Deep Learning Algorithms for Object Detection - https://medium.com/zylapp/review-of-deep-learning-algorithms-for-object-detection-c1f3d437b852
 - Object Detection - Papers with Code - https://paperswithcode.com/task/object-detection
 - R. Girshick, J. Donahue, T. Darrell, J. Malik, Rich feature hierarchies for accurate object detection and semantic segmentation - Original article of R-CNN - https://arxiv.org/abs/1311.2524
 - R. Girshick, Fast R-CNN - Original article of Fast R-CNN - https://arxiv.org/abs/1504.08083
 - Original implementation of Fast R-CNN - https://github.com/rbgirshick/fast-rcnn
 - S. Ren, K. He, R. Girshick, J. Sun, Faster R-CNN: Towards Real-Time Object Detection with Region Proposal Networks - Original article of Faster R-CNN - https://arxiv.org/abs/1506.01497
 - Faster R-CNN ResNet-50-FPN - PyTorch Trained Model trained on COCO 2017 - https://pytorch.org/docs/stable/torchvision/models.html#faster-r-cnn
 - Detectron, GitHub - https://github.com/facebookresearch/Detectron
 - Faster R-CNN: Towards Real-Time Object Detection with Region Proposal Networks - Original implementation of Faster R-CNN - https://github.com/ShaoqingRen/faster_rcnn
 - py-faster-rcnn - Original implementation of Faster R-CNN - https://github.com/rbgirshick/py-faster-rcnn
 - K. He, G. Gkioxari, P. Dollár, R. Girshick, Mask R-CNN - Original article of Mask R-CNN - https://arxiv.org/abs/1703.06870
 - Mark R-CNN ResNet-50-FPN - PyTorch Trained Model trained on COCO 2017 - https://pytorch.org/docs/stable/torchvision/models.html#mask-r-cnn
 - J. Redmon, S. Divvala, R. Girshick, A. Farhadi, You Only Look Once: Unified, Real-Time Object Detection - Original article of YOLO - https://arxiv.org/abs/1506.02640
 - YOLO v3 - Darknet Trained Model trained on COCO 2014 - https://pjreddie.com/darknet/yolo/
 - YOLO v2 - Darknet Trained Model trained on COCO 2014 - https://pjreddie.com/darknet/yolov2/
 - YOLO v1 - Darknet Trained Model trained on Pascal VOC 2007+2012 - https://pjreddie.com/darknet/yolov1/
 - W. Liu, D. Anguelov, D. Erhan, C. Szegedy, S. Reed, C.-Y. Fu, A. C. Berg, SSD: Single Shot MultiBox Detector - Original article of SSD - https://arxiv.org/abs/1512.02325
 - SSD 300 - Caffe Trained Model trained on COCO 2015, Pascal VOC 2007+2012 and Pascal VOC 2007+2012+COCO 2015 - https://github.com/weiliu89/caffe/tree/ssd#models
 - SSD 512 - Caffe Trained Model trained on COCO 2015, Pascal VOC 2007+2012 and Pascal VOC 2007+2012+COCO 2015 - https://github.com/weiliu89/caffe/tree/ssd#models
 - SSD: Single Shot MultiBox Detector in TensorFlow - https://github.com/balancap/SSD-Tensorflow
 - Gluon - Predict with pre-trained SSD models - https://gluon-cv.mxnet.io/build/examples_detection/demo_ssd.html
 - MXNet - SSD: Single Shot MultiBox Object Detector - https://modelzoo.co/model/ssd
 - PyTorch Hub - SSD - https://pytorch.org/hub/nvidia_deeplearningexamples_ssd/
 - X. Zhou, D. Wang, P. Krähenbühl, Objects as Points - Original article of CenterNet (Objects as Points) - https://arxiv.org/abs/1904.07850v2
 - Objects as Points - Original implementation of CenterNet (Objects as Points) - https://github.com/xingyizhou/CenterNet
 


