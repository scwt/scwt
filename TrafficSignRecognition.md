# Traffic Sign Recognition

## Papers
 - Traffic Sign Recognition - Papers with Code - https://paperswithcode.com/task/traffic-sign-recognition
 - Detectron for Traffic Signs - https://github.com/skokec/detectron-traffic-signs
 - Hoanh Nguyen. Fast Traffic Sign Detection Approach Based on Lightweight Network and Multilayer Proposal Network - https://www.hindawi.com/journals/js/2020/8844348/
 - R. Nagpal et al. Real-time traffic sign recognition using deep network for embedded platforms - https://www.researchgate.net/publication/336116814_Real-time_traffic_sign_recognition_using_deep_network_for_embedded_platforms
 - Y. Jin, Y. Fu, W. Wang, J. Guo, C. Ren and X. Xiang. Multi-Feature Fusion and Enhancement Single Shot Detector for Traffic Sign Recognition - https://ieeexplore.ieee.org/document/9007360, https://www.researchgate.net/publication/339480727_Multi-Feature_Fusion_and_Enhancement_Single_Shot_Detector_for_Traffic_Sign_Recognition
 - Benhe Gao, Zhongjun Jiang, Jiaman Zhang. Traffic Sign Detection based on SSD. - https://www.researchgate.net/publication/336003722_Traffic_Sign_Detection_based_on_SSD
 - Shu-fang Zhang and Tong Zhu. Traffic sign detection and recognition based on residual single shot multibox detector model - http://www.zjujournals.com/eng/EN/10.3785/j.issn.1008-973X.2019.05.015
 - O. Chumachenko, L. Petryshyn and  V. Konchinsky, Traffic Sign Detection and Recognition Using Single Shot Multibox Detector - https://jrnl.nau.edu.ua/index.php/ESU/article/view/15582

## Tutorials
 - Traffic Sign Classification with Keras and Deep Learning - https://www.pyimagesearch.com/2019/11/04/traffic-sign-classification-with-keras-and-deep-learning/
 - Traffic sign recognition using deep neural networks - https://towardsdatascience.com/traffic-sign-recognition-using-deep-neural-networks-6abdb51d8b70
 - Recognizing Traffic Signs with CNNs - https://medium.com/@thomastracey/recognizing-traffic-signs-with-cnns-23a4ac66f7a7
 - Implementing Single Shot Detector (SSD) in Keras: Part I — Network Structure - https://towardsdatascience.com/implementing-ssd-in-keras-part-i-network-structure-da3323f11cff
 - SSD in TensorFlow: Traffic Sign Detection and Classification - https://github.com/georgesung/ssd_tensorflow_traffic_sign_detection
 
