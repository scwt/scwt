## Similar projects:

### Free or Open-source:
 - Labelimg – https://github.com/heartexlabs/labelImg
 - Label Studio – https://labelstud.io/
 - CVAT (Computer Vision Annotation Tool) – https://cvat.ai/
 - VoTT (Visual Object Tagging Tool) – https://github.com/microsoft/VoTT
 - LabelMe – http://labelme.csail.mit.edu/Release3.0/
 - Img Lab – https://imglab.in/

### Paid:
 - V7 – https://www.v7labs.com/
 - Labelbox – https://labelbox.com/
 - Scale AI – https://scale.com/
 - SuperAnnotate – https://www.superannotate.com/
 - Dataloop – https://dataloop.ai/
 - Playment – https://www.telusinternational.com/solutions/ai-data-solutions?INTCMP=ti_playment
 - Supervise.ly – https://supervise.ly/
 - Hive Data – https://thehive.ai/

## Annotated Image Datasets
 - Helen Dataset - http://www.ifp.illinois.edu/~vuongle2/helen/
 - Annotated Facial Landmarks in the Wild (AFLW) - https://www.tugraz.at/institute/icg/research/team-bischof/lrs/downloads/aflw/
 - Large-scale CelebFaces Attributes (CelebA) Dataset - http://mmlab.ie.cuhk.edu.hk/projects/CelebA.html
 
## Active Appereance Models
 - Активные модели внешнего вида - https://habr.com/ru/post/155759/

## Face Recognition Algorithms based on AAM and Annotated Datasets
 - Dlib Real-Time Face Pose Estimation - http://blog.dlib.net/2014/08/real-time-face-pose-estimation.html
 - CLandmark (Open Source Landmarking Library) - http://cmp.felk.cvut.cz/~uricamic/clandmark/
 
## Git Basics
 - Основи використання системи контролю версій Git - https://gitlab.com/itpm/itpmanagement/blob/master/Lab1.pdf
