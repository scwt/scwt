## Papers
 - F. Schroff, D. Kalenichenko, J. Philbin. FaceNet: A Unified Embedding for Face Recognition and Clustering - 
 [PDF](https://arxiv.org/pdf/1503.03832.pdf) [Resource Page](https://arxiv.org/abs/1503.03832)
 - Face Recognition - Papers with Code - https://paperswithcode.com/task/face-recognition
 - O. M. Parkhi, A. Vedaldi and A. Zisserman, Deep Face Recognition - http://www.bmva.org/bmvc/2015/papers/paper041/index.html

## APIs

 - Top Image Processing and Facial Recognition APIs - https://rapidapi.com/collection/top-image-recognition-apis?page=1
 - Top 10 Facial Recognition APIs & Software of 2020 - https://rapidapi.com/blog/top-facial-recognition-apis/

## Existing Projects
 - OpenFace - Проект написаний на Torch (Lua) з інтерфейсом на Python, має веб-демо, яке може бути використане як основа 
 для розробки інтерфейсу - https://cmusatyalab.github.io/openface/ [GitHub Page](https://github.com/cmusatyalab/openface)
 - Face Recognition using TensorFlow - Альтернативна реалізація FaceNet повністю на TensorFlow (Python) - 
 https://github.com/davidsandberg/facenet
 - Other Alternative FaceNet Implementations - 
 [google Search](https://www.google.com/search?q=facenet+github&client=firefox-b-e&ei=zPhdXrr6MKPprgTe4YOIBg&start=0&sa=N&ved=2ahUKEwi6kIyt1v3nAhWjtIsKHd7wAGE4ChDy0wN6BAgMEC8&biw=1280&bih=885) 
 [GitHub Topics](https://github.com/topics/facenet)
 - Deep Face Recognition with Keras - https://sefiks.com/2018/08/06/deep-face-recognition-with-keras/
 - Face Recognition with OpenFace in Keras - https://sefiks.com/2019/07/21/face-recognition-with-openface-in-keras/
 - Face Recognition with FaceNet in Keras - https://sefiks.com/2018/09/03/face-recognition-with-facenet-in-keras/
 - Face Recognition with Facebook DeepFace in Keras - https://sefiks.com/2020/02/17/face-recognition-with-facebook-deepface-in-keras/
 - Face Recognition with DeepID in Keras - https://sefiks.com/2020/06/16/face-recognition-with-deepid-in-keras/

## Git Basics
 - Основи використання системи контролю версій Git - https://gitlab.com/itpm/itpmanagement/blob/master/Lab1.pdf
 - git-submodule Documentation - https://git-scm.com/docs/git-submodule (
 [рос. версія статті](https://git-scm.com/book/ru/v2/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B-Git-%D0%9F%D0%BE%D0%B4%D0%BC%D0%BE%D0%B4%D1%83%D0%BB%D0%B8))
 - Организация работы с git submodules [Хабр] - https://habr.com/ru/post/60860/
 - Git subtree в деталях [Хабр] - https://habr.com/ru/post/429014/
 - Git subtree: the alternative to Git submodule - https://www.atlassian.com/git/tutorials/git-subtree