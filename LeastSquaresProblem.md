## Non-linear Least Squares Methods
  - Задачин В. М., Конюшенко І. Г., ЧИСЕЛЬНІ МЕТОДИ, Навчальний посібник - http://kist.ntu.edu.ua/textPhD/CHM_Zadachin.pdf - c. 66 - 
Приклад простого розв'язку СНР двох змінних використовуючи пакет R (використовується готовий оптимізаційний метод
https://www.rdocumentation.org/packages/stats/versions/3.6.1/topics/optim)
  - Дэннис Дж., Шнабель Р. Численные методы безусловной оптимизации и решения нелинейных уравнений - https://www.twirpx.com/file/634968/ 
(v2 - https://edu-lib.com/matematika-2/dlya-studentov/dennis-dzh-shnabel-r-chislennyie-metodyi-bezuslovnoy-optimizatsii-i-resheniya-nelineynyih-uravneniy-onlayn)
([Direct Link](https://gitlab.com/scwt/scwt/blob/4d46f5cec3f632c43ab15a41f2b8b2fae3ba7898/books/dennis_dzh_shnabel_r_chislennye_metody_bezuslovnoy_optimizat.djvu)) - Розділ 10 -
Теорія методу МНК для СНР
  - Björck Å. Numerical Methods for Least Squares Problems - https://www.twirpx.com/file/1160854/
([Direct Link](https://gitlab.com/scwt/scwt/blob/c6ca404c19b2dc83e0285892aed7eda2fec259fa/books/bjoerck_a_numerical_methods_for_least_squares_problems.djvu) - Chapter 9 -
Теорія методу МНК для СНР
  - Gauss–Newton algorithm - https://en.wikipedia.org/wiki/Gauss%E2%80%93Newton_algorithm - Алгоритм для розв'язку задачі МНК для СНР
  - Levenberg–Marquardt algorithm - https://en.wikipedia.org/wiki/Levenberg%E2%80%93Marquardt_algorithm - Алгоритм для розв'язку задачі МНК для СНР
  - Ортега Джеймс, Рейнболдт Вернер. Итерационные методы решения нелинейных систем уравнений со многими неизвестными - https://www.twirpx.com/file/153808/ 
(v2 - https://edu-lib.com/matematika-2/dlya-studentov/ortega-dzh-reynboldt-ver-iteratsionnyie-metodyi-resheniya-nelineynyih-sistem-uravneniy-so-mnogimi-neizvestnyimi-onlayn)
(v3 - https://ikfia.ysn.ru/wp-content/uploads/2018/01/OrtegaRejnboldt1975ru.pdf) - Метод Гауса-Ньютона і пов'язана з ним теорія

## Other Sources
  - Algorithms for Linear Least Squares Problems - https://www.researchgate.net/publication/268856866_Algorithms_for_Linear_Least_Squares_Problems
  - Численное решение задач метода наименьших квадратов - Лоуcон Ч. - http://booksshare.net/index.php?id1=4&category=math&author=louson-ch&book=1986